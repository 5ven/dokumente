---
vim: sw=4 tw=90
FIXME documentclass: s0minutes
documentclass: scrartcl
header-includes:
  - |
    \usepackage{longtable}
    \usepackage{booktabs}
    \usepackage[default]{droidsans}
papersize: a4
lang: de
numbersections: true
secnumdepth: 2
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2020-01-26
s0minutes:
  typeofmeeting: \generalassembly
  date: 2020-01-26
  place: Stratum 0, Hamburger Straße 273a, Braunschweig
  startingtime: 14:24
  endtime: 17:30
  attendants:
    proper-members: 37
    entitled-to-vote: 36
    supporting-members: 1
  absentees: 
  minutetaker: rohieb
---

Protokoll-Overhead
==================

### Wahl der Versammlungsleitung
Es wird larsan vorgeschlagen.
Es gibt keine Einwände gegen larsan.
larsan leitet somit die Versammlung.

### Wahl der Protokoll führenden Entität
Es wird rohieb vorgeschlagen.
Es gibt dagegen keine Einwände.
rohieb schreibt also ein Protokoll.

### Zulassung von Gästen
Es sind keine Gäste anwesend.

### Feststellung der Beschlussfähigkeit
Der Verein hat zum heutigen Stichtag 13 Fördermitglieder und 100 ordentliche
Mitglieder, insgesamt also 113 Mitglieder.
Satzung, § 7 Abs. 6 fordert Anwesenheit von mindestens 23% der ordentliche
Mitglieder für eine beschlussfähige Mitgliederversammlung.
23% von 100 sind 23.
Es sind 37 ordentliche Mitglieder anwesend.
Das sind mehr als 23 ordentliche Mitglieder.
Die Versammlung ist somit beschlussfähig.

### Quoren
Das Quorum für die Annahme eines Antrags (Satzung, §7 Abs. 7) beträgt mehr als
50% der anwesenden ordentlichen Mitglieder.
50% von 37 sind 18,5.
Ein Antrag gilt also mit 19 Pro-Stimmen als angenommen.

### Abstimmung über die Geschäftsordnung
Die [Geschäftsordnung der Mitgliederversammlung][gomv] wird unverändert und ohne Einwände
angenommen.

[gomv]: https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung


Berichte
========

Finanzbericht
-------------

Der Emantor berichtet als Schatzmeister vom vergangenen Geschäftsjahr.
Seine Präsentation ist [im Internet][fb2020] zum Nachlesen verfügbar.

[fb2020]: https://stratum0.org/wiki/Datei:Finanzbericht_2020.pdf

Es gab dieses Jahr vierteljährliche Kassenprüfungen mit den Rechnungsprüfern, um den
Aufwand direkt vor der Mitgliederversammlung gering zu halten und übers Jahr auf mehrere
kürzere Sitzungen zu verteilen, und von den Rechnungsprüfern schneller und öfters Feedback
über mögliche Probleme zu erhalten.
Dieses Modell hat sich laut allen Beteiligten bewährt und wird dem nächsten Vorstand
zur Weiterführung empfohlen.

Firmenspenden gab es im vergangen Jahr (leider) keine,
allerdings haben seit Januar 2020 viele Einzelpersonen für die Anschaffung eines
Laser-Cutters gespendet (dazu später mehr).

### Vorhandenes Kapital
Im Moment (Januar 2020) verfügt der Verein über ein Gesamtkapital von über 20.000€.
Darin enthalten sind auch schon die genannten Spenden für einen Laser-Cutter.
Größere Änderungen im Bestand gab es am Anfang des Geschäftsjahres durch eingehende
Mitgliedsbeiträge, und im Oktober wurde ein neuer Beamer für 2.100 € angeschafft.

Kontenstände zum Schluss des Geschäftsjahres:

\begin{longtable}{llr}
  \textbf{Nr.} & \textbf{Name} & \textbf{Bestand zum 31.12.2019 [€]} \\
  \toprule
  \endfirsthead
  100          & Barkasse                              &      657,31 \\
   100-2       & Pfand für physische Schlüssel         &      120,00 \\
   100-4       & Spenden für Stick-Material            &        0,00 \\
  \midrule
  101          & Erstattungskasse Verbrauchsmaterial   &      102,11 \\
  \midrule
  102          & Matekasse                             &      166,96 \\
  \midrule
  200024917    & Girokonto                             &   17.651,36 \\
   200024917-1 & Rückstellungen Girokonto              &    3.160,00 \\
   200024917-2 & Pfandgeld auf Girokonto               &      360,00 \\
  \bottomrule
               &  \textbf{Summe}                & \textbf{ 22.217,74 } \\
\end{longtable}

Das Pfandgeld für physische Schlüssel zum Space liegt nun teilweise auf dem Girokonto,
damit sich nicht so viel Bargeld im Space befindet, das bei einem möglichen Diebstahl
abhanden kommen könnte.

Folgende Rückstellungen bestehen:

* 160 € für Ausfall der Mietsicherheit (sollte schon lange aufgelöst worden sein, wurde
  aber bisher nicht umgesetzt)
* 3000 € um überraschende Einnahmeausfälle aufzufangen

### Einnahmen und Ausgaben
Das Kapital bewegt sich insgesamt relativ konstant, Einnahmen und Ausgaben halten sich die
Waage.

Durchschnittliche monatliche Einnahmen im letzten Jahr:

* 1642 € Mitgliedsbeiträge (etwas höher im Vergleich zum letzten Jahr)
* 138 € Spenden (niedriger im Vergleich zum letzten Jahr)
* weniger als je 30 € für 3D-Druck, Freifunk, Plotter, und Stickmaschine

Im Vergleich zum letzten Jahr gibt es einen leichten Aufwärtstrend bei den Einnahmen,
auch ohne die noch heute eingegangenen Mitgliedsbeiträge.

Durchschnittliche monatliche Ausgaben:

* etwa 1000 € für Miete, Strom und Nebenkosten (relativ konstant geblieben)
* etwa 20 € für Kontoführungsgebühren
* zwischen 0 € und 120 € für Verbrauchsmaterial (stark schwankend)

Hierzu die Erinnerung, dass eine viertel-, halb- oder jährliche Zahlung der
Mitgliedsbeiträge weniger Kontoführungsgebühren mit sich bringt, da jede eingehende
Überweisung den Verein 10 Cent kostet.

Es bestehen im Moment laufende Verpflichtungen von insgesamt mindestens 1112 € pro Monat:

* Miete: 730 €
* Strom: 285 €
* Internet und Telefon: 20 €
* Haftpflichtversicherung: 12 €
* Server: 64 €
* Domain stratum0.org: 1 €

### Mitgliederentwicklung
Die Mitgliederentwicklung stieg leicht von 105 Mitgliedern am Anfang des Jahres
bis auf 113 Mitglieder am Ende des Jahres.
Einige Mitglieder haben ihren Austritt erklärt, was aber durch genügend Neumitglieder
aufgefangen wurde.

Es wird vom Schatzmeister die Vermutung geäußert, dass viele Mitglieder inzwischen vom
Studentenstatus ins Erwerbsleben gewechselt sind, und somit die Mundpropaganda unter den
Studenten der TU Braunschweig fehlt.
Dies könnte man durch gezielte Werbung wiederbeleben.

### Matekasse
In der Mitte des Jahres gab es eine Party, was vermutlich der Grund für gestiegene
Einnahmen in der Getränkekasse ist.
Aus dieser Kasse wurden letztes Jahr etwa 600 € Gewinn entnommen.
Nichtsdestotrotz gibt es einige Leute, die mit 50 € oder mehr in der Kreide stehen und
ihre Schulden abbezahlen sollten.

Nach Gespräch mit dem Finanzamt wurde klargestellt, dass die Matekasse als
wirtschaftlicher Geschäftsbetrieb nur Zuschüsse zum Vereinsvermögen liefern und nicht von
anderen zweckgebundenen Einnahmen aus dem ideellen Teil des Vereins querfinanziert werden
darf.

### Verwendung von zweckgebundenen Spenden
Zweckgebundene Spenden für CoderDojo und Freifunk werden weiterhin gesondert in der
Buchführung ausgeführt, und beide Projekten können über die Beträge selbst verfügen.
Aus oben erwähntem Gespräch mit dem Finanzamt ergab sich hierzu auch, dass für
zweckgebundene Spenden, für die eine Zuwendungsbestätigung ausgestellt wurde, zwar eine
moralische Verpflichtung besteht, das Geld zweckmäßig zu verwenden; da jedoch die Spende
nicht zurückgegeben werden kann (da eine ausgestellte Zuwendungsbestätigung ja zu
Steuervorteilen bei der spendenden Person führt), kann sie auch für andere
steuerbegünstigte Zwecke verwendet werden, falls ihr ursprünglicher Zweck weggefallen ist.
Beispiel: für eine Spende mit dem Betreff „neuer Beamer“ wird eine Zuwendungsbestätigung
ausgestellt. Es stellt sich jedoch heraus, dass für die Anschaffung eines neuen Beamers
mehr Spenden eingegangen sind als die Anschaffungskosten. Die Spende darf somit auch für
andere Vereinszwecke verwendet werden.

### Erstattungskasse
Die Selbsterstattungskasse hat dieses Jahr eine neue Regelung bekommen.
In der Vergangenheit durften daraus nur Verbrauchs- und Putzmaterial beschafft werden,
seit diesem Jahr dürfen aber auch beliebige Anschaffungen bis 50 € mit der Zustimmung von
4 Mitgliedern getätigt werden, solange genügend Geld in der Kasse ist (genaue und aktuelle
Regelung im [Wiki][erstattungskasse]).

Beispiele aus der Vergangenheit für diesen Anwendungsfall sind:

* Eisenwaren für die neue Trittleiter in der Werkstatt (27 €)
* Bänder für den Dymo-Drucker (18 €)
* Rohlinge für die Button-Presse (30 €)
* Tonerchips für den Laser-Drucker (9 €)
* eine zertifizierte Granitmessplatte (49 €)
* Lüfter für 3D-Drucker (15 €)
* Rollensätze, z.B. als Unterbau für den Plotter-Schrank (13 €)
* Müllsäcke (4,50 €)
* Rollböcke für die Werkstatt (32 €)

[erstattungskasse]: https://stratum0.org/wiki/Erstattungskasse

### Ausblick
Die Einnahmen durch Mitgliedsbeiträge sind wie erwähnt im Aufschwung.

Für einen Laser-Cutter sind viele Spenden von Einzelpersonen, teilweise im dreistelligen
Bereich, eingegangen und werden dieses Jahr ausgegeben.

Der Stromverbrauch bewegt sich weiterhin recht konstant bei 11 MWh pro Jahr.
Hier ist durch die Anschaffung eines Laser-Cutters aber eine Steigerung zu erwarten.

Die Nebenkostenabrechnung für das Jahr 2018 ist eingegangen, und wir erwarten hieraus eine
Erstattung von 80 €.

### Fragen

* Kontogebühren sind ja sehr ärgerlich, kann man da was machen?
    * Ein Wechsel zu einer anderen Bank wurde mehrmals im Vorstand diskutiert,
      es wurden auch andere Modelle der Sparkasse schon durchgerechnet, es gibt
      allerdings im Moment kein Konto, was sinnvoll billiger ist.
      Zudem sind wir auf eine Möglichkeit zur Bareinzahlung (Matekasse, Barkasse)
      angewiesen, die durch Filialbanken sichergestellt wird.
    * Mitglieder können wie erwähnt ihre Daueraufträge auf eine niedrigere Frequenz
      umstellen, um die Anzahl eingehender Buchungen auf dem Konto zu verringern.


Bericht der Rechnungsprüfer
---------------------------

Angela und shoragan berichten als Rechnungsprüfer.

Wie im Finanzbericht schon erwähnt, werden vierteljährliche Kassenprüfungen sehr empfohlen.

Es gab dieses Jahr nur zwei kleinere To-Dos:
Einmal wurden 33 € an Nebenkosten zuviel überwiesen.
Das andere ist ein fehlender Papier-Kontoauszug von letztem Jahr.
Beide Punkte befinden sich schon in Bearbeitung.

Von den Rechnungsprüfern wird auch wieder angesprochen, dass die Schulden im
Matekassen-System sich höchstens im einstelligen Euro-Bereich bewegen sollen.

Außerdem weisen sie darauf hin, dass Telefonate aus dem Space nicht kostenlos sind.
Ein entsprechender Hinweis auf die Spendenbox wurde am Telefon angebracht.


Tätigkeitsbericht des Vorstands
-------------------------------

Über die Tätigkeit des Vorstandes im vergangenen Jahr berichtet Chrissi^.
Seine Präsentation ist [auf der Homepage][tb2020] einsehbar.

[tb2020]: https://stratum0.org/wiki/Datei:T%C3%A4tigkeitsberichtMV2020.pdf

### Struktur
Chrissi^ erläutert die einzelnen Ämter im Verein.
Von der letzten Mitgliederversammlung bestellt wurden:

* zwei Rechnungsprüfer, die schon vorher im Finanzbericht erwähnt wurden,
* Daniel Sturm als Vertrauensperson, welcher heute nicht anwesend ist, aber einen Bericht
  geschickt hat.

Von der letzten Mitgliederversammlung gewählt wurde der Vorstand:

* Vorstandsvorsitzender: Lars Andresen (larsan)
* stellv. Vorsitzender: René Stegmaier (reneger)
* Schatzmeister: Rouven Czerwinski (Emantor)
* Beisitzerin: Linda Fliß (Chamaeleon)
* Beisitzer: Daniel Thümen (Chaosrambo)
* Beisitzer: Chris Fiege (chrissi^)

Als Vorstand vertretungsberechtigt sind dabei gemeinsam mindestens zwei Personen aus der
Menge Vorstandsvorsitzender, stellv. Vorsitzender, Schatzmeister.
Die Vertretung des Schatzmeisters gem. [Geschäftsordnung des Vorstandes][gov] wurde von
der Beisitzerin Linda wahrgenommen.

[gov]: https://stratum0.org/mediawiki/index.php?oldid=23617

Die Verwaltung von Schlüsseln (physisch und SSH-Zugang) geschah durch den
Vorstandsvorsitzenden larsan.
Die Mitgliederverwaltung geschah durch Schatzmeister Emantor und Beisitzer Chrissi^.
Die Schriftführung in den Vorstandssitzungen übernahm Beisitzer Chrissi^.

### Statistik
Dieses Vorstandsjahr gab es 5 Vorstandssitzungen, die insgesamt auf 35 DIN-A4-Seiten
protokolliert wurden.
Auf der Mailingliste des Vorstands wurden über 1250 Mails in über 450 Threads geschrieben
(Stand Mitte dieser Woche).
Dies entspricht in etwa dem Wert der Vorjahre.
Es wurden 47 Umlaufbeschlüsse eingebracht, was etwas weniger als in den Vorjahren sind.
Die Verringerung ist auf die Einführung der Selbsterstattungskasse zurückzuführen.

### Steuererklärung
Es wurde eine Steuererklärung abgegeben, welche für Vereine unserer Größe alle drei Jahre
fällig ist.
Das Finanzamt schickt dazu rechtzeitig eine Aufforderung zur Abgabe der Steuererklärung.
Die Gemeinnützigkeit wurde vom Finanzamt wie zuvor rückwirkend für den Zeitraum der
letzten drei Jahre festgestellt.
Die rückwirkende Feststellung birgt jedoch prinzipiell die Gefahr, dass Spenden im Falle
einer wegfallenden Steuerbegünstigung den jeweiligen Spendern zurück erstattet werden
müssen, was die Verwaltung von Spenden aufwändig macht.

Die Steuererklärung umfasste dieses Mal 191 Seiten, 120 MB als PDF (im Vergleich dazu
vorheriges Mal nur 156 Seiten).
Das Finanzamt wies außerdem darauf hin, dass nicht so viele Unterlagen eingereicht werden
müssten;
es genügten Gewinn-Verlust-Rechnung und Protokolle der Mitgliederversammlungen.
Sollte die nächste Steuererklärung wieder auf Papier notwendig sein, sollte darauf
geachtet werden, nur die tatsächlich notwendigen Unterlagen einzureichen.
Der Schatzmeister wird aufgefordert, ein „How To Steuererklärung“ zu schreiben.

### Vorstandsprotokolle
Die Protokolle der Vorstandssitzungen werden ohne eventuelle datenschutzrelevante
Informationen der Vereinsöffentlichkeit [als PDF im Wiki][kvs] zur Verfügung gestellt.
Die vollständigen Protokolle werden intern nur für den Vorstand zugänglich abgelegt.

[kvs]: https://stratum0.org/wiki/Kategorie:Vorstandssitzungen

### Inhaltsversicherung
Weiterhin hat sich der Vorstand Gedanken über eine Inhaltsversicherung zusätzlich zur
bereits bestehenden Haftpflichtversicherung gemacht.
Hintergrund ist der Wunsch auf Absicherung im Falle von Elementarschäden oder Diebstahl.
Jedoch ist die nötige Versicherungssumme nicht trivial festzustellen, da sich in den
Vereinsräumlichkeiten auch viele Gegenstände von Mitgliedern befinden.
Es wurde daher bisher kein Beschluss gefasst.

### Laser-Cutter
Zu Beginn des Jahres wurde eine Arbeitsgruppe gegründet, die Anforderungen an einen
Laser-Cutter aufgestellt und mögliche Angebote eingeholt hat.
Nach der Sommerpause wurde als favorisiertes Modell ein 100 W CO~2~-Laser mit Arbeitsfläche
von 100 cm × 800 cm ins Auge gefasst.
Dieses Modell soll zu Kosten von 11.100 € angeschafft werden.
Zusätzlich werden etwa 1.500 € für Abluft, Zubehör, Transport und Einbau in den Space
veranschlagt.
Aufgrund der Größe des Gerätes muss etwa die Tür zum Frickelraum entfernt und wieder
eingesetzt werden, und Kernbohrungen für Abluft gesetzt werden.
Stand 12/2019 verfügt der Verein über mehr als 13.000 € ungebundenes Kapital auf dem Konto.

Der Vorstand hat in [UB 2019-12-16-01][vs20191206] beschlossen, 12.600 € für die
Anschaffung des Laser-Cutters inkl. Nebenkosten bereitzustellen, falls 3000 € durch Spenden
zusammen kommen.

[vs20191206]: https://stratum0.org/wiki/Vorstandssitzung_2019-12-16

Es wird die Frage gestellt, was man mit einem Laser-Cutter machen könne.
Das favorisierte Modell könne benutzt werden um z.B. Sperrholz, Glas, Leder, Papier,
Plastik (falls ungiftig), aber kein Metall, zu schneiden, sowie um Materialien (inkl.
Metall) zu gravieren.
Die maximalen Bearbeitungsmaße betragen dabei etwa 28 cm in der Höhe, 100 cm in der Breite
und 80 cm in der Länge.
Aufgrund der von uns gewählten Laserklasse 1 muss der Laserstrahl entsprechend im Gehäuse
abgeschirmt sein, somit wäre kein Durchschieben von Material möglich.

Es wird gefragt, ob sich die Anschaffung des Laser-Cutters überhaupt für den Verein lohne.
Dazu wird angemerkt, dass die Idee, einen Laser-Cutter anzuschaffen, seit etwa 7 Jahren im
Raum stehe.
Das erwartete Nutzungsverhalten werde sich nach Vergleich mit Laser-Cuttern in anderen
Hackspaces und unseren 3D-Druckern wahrscheinlich am Anfang nach der Anschaffung häufen,
aber später dann auf einen konstanten Bereich absinken.
Zudem habe die Arbeitsgruppe auch nach gebrauchten Laser-Cuttern recherchiert, mit dem
Ergebnis, dass ein solches Gerät relativ wertstabil und im Notfall auch schnell wieder zu
verkaufen sei.
Als Wartungsintervall wird vom Hersteller durchschnittlich 100.000 Stunden angegeben,
wovon aber die Laserquelle als Verbrauchsmaterial ausgeschlossen ist.

### Space 3.0
Es kommt die Frage auf, ob eine Vergrößerung des Spaces oder auch ein Umzug weiterhin
verfolgt würden.
Das sei weiterhin der Fall; einige Leute beobachteten weiterhin den Markt, es gäbe nur im
Moment nichts passendes in Bezug auf Lage und Finanzierbarkeit.
Für eine Räumlichkeit im Bereich von etwa 200 m² seien aktuell etwa 2000–3000 € an Miete
und Nebenkosten zu veranschlagen.

Möglicherweise gibt es im Schimmel-Hof einen freien Kellerraum, der im Moment als
zusätzlicher Lagerraum in Betracht gezogen wird.
In dieser Sache gibt es aber noch keine konkreten Ergebnisse.

### Sonstige Themen
Die neue Regelung zur Selbsterstattungskasse wurde im Finanzbericht bereits erwähnt.

Mangels Nachfrage und wegen aufwändigem Workflow gibt es keine Mitgliedsanträge auf Papier
mehr.
Mitgliedsanträge können weiterhin formlos per Mail gestellt werden.

Arbeitstreffen zur Geschäftsführung finden weiterhin an jedem ersten Dienstag im Monat
statt.
Diese sind explizit auch für alle Vereinsmitglieder offen und dienen dazu, Aufgaben mit
Vereinsbezug zu erledigen, die nicht unbedingt nur vom Vorstand erledigt werden müssen.
Beispiele für in der Vergangenheit behandelte Themen sind Öffentlichkeitsarbeit,
Diskussionen bzgl. Anschaffungen und Regelungen zu zwischenmenschlichem Zusammenleben im
Space, entsprechende Anträge verfassen, Firmenspenden einwerben, Vorbereitung der
Mitgliederversammlung, Suche nach neuen Räumlichkeiten, Vorantreiben der
Finanzbericht-Automatisierung, etc.

Der Verein ist nun beim OLG Oldenburg in das [„Niedersächsische Verzeichnis der
gemeinnützigen Einrichtungen als Empfänger von Geldauflagen in Ermittlungs-, Straf- und
Gnadenverfahren“][NvdgEaEvGiESuG] eingetragen.

[NvdgEaEvGiESuG]: https://oberlandesgericht-oldenburg.niedersachsen.de/service/verzeichnis_gemeinnuetzigen_einrichtungen/verzeichnis-der-gemeinnuetzigen-einrichtungen-80000.html


Jahresbericht
-------------

larsan hat eine Präsentation mit vielen Bildern über das vergangene Jahr vorbereitet, die
[im Wiki][jb2020] verfügbar ist.

[jb2020]: https://stratum0.org/wiki/Datei:JahresberichtMV2020.pdf

### Gruppen und regelmäßige Veranstaltungen
Der Space ist wie auch letztes Jahr schon ziemlich voll.
Es finden mehr als 30 Termine pro Monat statt, darunter:
Vorträge, Freifunk, CoderDojo, Digitalcourage/Autonymes Reisen, Coptertreff, Malkränzchen,
C64-Demogruppe, und gemeinsame Treffen mit ags und Akafunk.
Die Termine Home Automation, Schreibtreff, Anime Referat und Captain's Log finden offenbar
nicht mehr statt.

Dieses Jahr gab es in jedem Monat einen Vortrags-Termin, und insgesamt wurden 40 Vorträge
gehalten, von denen auch von den meisten eine Aufzeichnung verfügbar ist.
Eine Möglichkeit zum Live-Streaming wird weiter angedacht.

Freifunk trifft sich jeden Mittwoch, und das Netz besteht inzwischen aus mehr als 400
Routern, mit weiterem Trend nach oben.
Das Event-WLAN kam auf mehreren Veranstaltungen zum Einsatz.
Für die Zukunft gibt es Pläne der Stadt, Feuerwehren und Jugendzentren mit Freifunk
auszustatten.
Das Projekt Parker zur Restrukturierung des Netzes mit neueren Technologien befindet sich
inzwischen im Beta-Betrieb, und das Konzept wurde auf mehreren Konferenzen vorgestellt.

Vom CoderDojo gab es, wie auch die letzten Jahre schon, 11 Termine, davon fanden vier bei
externen Unterstützern (msg DAVID, Triology, cloudogu, PROSPER-X) statt.
Fürs CoderDojo werden weiterhin immer Mentoren gesucht!

### Workshops
Es gab dieses Jahr Workshops zu Pflanzenmilch-Blind-Verkostung, Wurst machen, Bier brauen,
ein Virtual-Reality-Wochenende, sowie einen Nachmittag, um DSGVO-Selbstauskünfte zu
verschicken.
Mehr Workshops sind immer gern gesehen :-)

### Infrastruktur
In diesem Themenbereich gab es kaum neues:
Das Admin-Team hat ein Review durchgeführt, wer eigentlich Zugriff auf welche Server hat,
und die Zuständigkeiten bei Bedarf neu verteilt.
Das NAS im Space („trokn“) hat ein neues Gehäuse bekommen.
Die Software für die digitale Strichliste der Getränkekasse wird gerade neu geschrieben.

### 8 Jahre Stratum 0
Zur Jubiläumsfeier _0b1000_ am 6. Juli wurden 96 (kostenlose) Tickets vergeben, und es gab
zwei Sorten selbst gebrautes Bier sowie andere Köstlichkeiten.

### Weitere Events
Auswärts war der Stratum 0 durch Abordnungen verschiedener Größe bei der Easterhegg in
Wien, bei der Gulaschprogrammiernacht in Karlsruhe, beim Chaos Communication Camp in
Mildenberg, und beim 36c3 in Leipzig vertreten.
Beim Camp haben wir uns wieder mit einigen anderen Spaces und Gruppen zur [Singularity
City][sc] zusammengeschlossen.

Dieses Jahr gab es wieder einen Space-Bau-Abend, was zur Folge hatte, dass man jetzt die
Kühlschrank-Temperatur über WLAN abfragen kann.

[sc]: https://singularitycity.de/

### Kommunikation
Es gab eine Ausgabe StratumNews, das ist mehr als gar keine! :-)
Wie immer darf es auch gerne mehr Blogposts auf unserem [Blog][] geben.
Es ist auch gar nicht schwer, nur ein bisschen Text mit ein bisschen Formatierung in
Markdown :-)

[Blog]: https://stratum0.org/blog

### Ausblick 2020
Ein Wort: L-A-S-E-R!
Die Suche nach Space 3.0 ist weiterhin Thema, und mehrere Entitäten halten weiterhin die
Augen nach geeigneten Räumlichkeiten offen.
Auch 2020 soll es wieder ein Hacken Open Air geben.
Außerdem ist wieder eine Bestellung für T-Shirts mit Stratum-0-Logo geplant.


Bericht der Vertrauensperson
----------------------------

Die Vertrauensperson ist nicht anwesend, aber hat reneger einen Bericht zukommen lassen.
Sie berichtet, dass es nichts zu berichten gibt, weil es im letzten Jahr keine Anfragen
gab und keine Anlässe tätig zu werden.

Es wird gefragt, ob es in dem Konflikt, von dem auf der letzten Mitgliederversammlung
berichtet wurde, weitere Vorkommnisse gab.
Dies sei laut Vorstand nicht der Fall.
Der Vorstand sieht weiterhin keinen Handlungsbedarf in dieser Sache, da es sich um
Privatangelegenheiten zwischen einzelnen Mitgliedern handele;
und sieht auch den zeitlichen Aufwand durch die Beschäftigung mit dieser Sache nicht als
zielführend an.

Jemand meint, dass persönliche Daten von Mitgliedern im Chat veröffentlicht wurden.
Der Vorstand wisse davon nichts, und sei auch nicht dazu angesprochen oder aktiv zu
einer Tätigkeit aufgefordert worden.


Entlastung des Vorstands
========================

Beide Kassenprüfer empfehlen die Entlastung des Vorstands.
Daraufhin wird der Antrag gestellt, den Vorstand für die vergangene Wahlperiode zu
entlasten.
Es sind 31 stimmberechtigte Mitglieder anwesend; der Vorstand selbst stimmt
regelgerecht nicht ab.

Dem Antrag auf Entlastung des Vorstandes wird einstimmig stattgegeben.
Damit ist der Vorstand für die vergangene Wahlperiode entlastet.

Wahlen
======

Emantor wird als Wahlleiter vorgeschlagen, dagegen gibt es keine Einwände.
Die Versammlungsleitung geht an die Wahlleitung, die den Wahlmodus laut
[Geschäftsordnung der Mitgliederversammlung][gomv] erläutert.

[gomv]: <https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung>

Es werden Kandidaten für die Vorstandsposten und die Rechnungsprüfer gesucht.
Die Rechnungsprüfer möchten weiterhin im Amt bleiben.
Es gibt keine Gegenkandidaten für das Amt der Rechnungsprüfer, sodass die Wahl
entfällt und shoragan und Angela im Amt bleiben.

Die zur Vorstandswahl kandidierenden Mitglieder stellen sich kurz vor.

Es wird geheim mit den vorher ausgegebenen Stimmzetteln gewählt.
Das Ergebnis wird um 17:23 verkündet.
Es gab 35 gültige Stimmzettel.
Es werden nur die Stimmenanzahlen der gewählten Kandidierenden vorgelesen:

* Als __Vorstandsvorsitzender__ ist larsan (Lars Andresen) mit 30 Stimmen (85% Zustimmung)
  gewählt.
  Er nimmt die Wahl an.
  
* Chrissi^ zieht seine Kandidatur zum stellvertretenden Vorsitzenden zurück.
  
* Als __stellvertretende Vorsitzende__ ist Feli (Felicitas Jung) mit 26 Stimmen (74%
  Zustimmung) gewählt.
  Sie nimmt die Wahl an.
  
* Als __Schatzmeister__ ist ktrask (Helena Schmidt) mit 30 Stimmen (85% Zustimmung) gewählt.
  Sie nimmt die Wahl an.
  
* larsan hat in der Wahl zum Beisitzer 31 Stimmen erreicht, ist aber schon als
  Vorstandsvorsitzender gewählt und scheidet damit als Beisitzer aus.
  
* Feli hat in der Wahl zur Beisitzerin 30 Stimmen erreicht, ist aber schon als
  stellvertretende Vorsitzende gewählt und scheidet damit als Beisitzerin aus.
  
* Als __Beisitzer__ ist Chrissi^ (Chris Fiege) mit 29 Stimmen (82% Zustimmung) gewählt.
  Er nimmt die Wahl an.
  
* Als __Beisitzer__ ist Linda Fliß mit 29 Stimmen (82% Zustimmung) gewählt.
  Sie nimmt die Wahl an.
  
* Als __Beisitzer__ ist rohieb (Roland Hieber) mit 27 Stimmen (77% Zustimmung) gewählt.
  Er nimmt die Wahl an.

Die Versammlungsleitung wird zurück an den Versammlungsleiter übergeben.


Sonstiges
=========

Es besteht die Möglichkeit, eine neue Vertrauensperson zu benennen.
Dies wird jedoch nicht gewünscht, sodass Daniel Sturm weiterhin als Vertrauensperson
zur Verfügung stehen wird, womit er auch einverstanden ist.

Als Termin für die nächste Mitgliederversammlung wird __Sonntag, der 17. Januar 2021__
beschlossen.

Es wird allen alten Vorstandsmitgliedern, die im neuen Vorstand kein Amt übernehmen,
gedankt.
Der neue und der alte Vorstand werden aufgefordert, im direkten Anschluss
an die Mitgliederversammlung einen Termin für die konstituierende Sitzung zu
vereinbaren.

Die Tagesordnungspunkte „Anerkennung des RaumZeitLabors als Außenstelle des Stratum 0“,
„Weltherrschaft“ und „Keysigning-Party“ werden aufgrund der fortgeschrittenen Zeit
vertagt.
