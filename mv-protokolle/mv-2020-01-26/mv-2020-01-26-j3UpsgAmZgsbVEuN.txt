*Mitgliederversammlung 2020-01-26


*TOP 0: Protokoll-Overhead
	* Beginn um 14:24
	* Wahl der Versammlungsleitung & Protokollführenden Entität
		* Versammlungsleitung: Vorschlag: larsan. keine Einwände.
		* Protokollführende Entität: rohieb. keine Einwände.
	* negative oder positive Feststellung der Beschlussfähigkeit
		* anwesend: 36 stimmberechtigte Mitglieder von 38 anwesenden Mitgliedern, davon 1 Fördermitglied, also 37 ordentliche Mitglieder anwesend
		* insgesamt: 113 Mitglieder Stand heute, davon 13 Fördermitglieder, also 100 ordentliche Mitglieder
		* 23% von 100 sind 23, es sind mehr als 23 stimmberechtigte Mitglieder anwesend => Versamlung beschlussfähig
		* Annahme eines Antrags: mindestens 50% der anwesenden ordentlichen Mitglieder = 18.5, also 19 Stimmen
		* (keine Satzungsänderungsanträge, also andere Quoren hinfällig)
	* Abstimmung über die Geschäftsordnung
		* https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung
		* keine Anträge zur GO

*TOP 1: Berichte
Es ist jetzt 14:30
	* Finanzbericht von Emantor
		* Übersicht und Neues
			* wie im letzten Jahr: Barkasse mit Pfandkasse und Speden für Stickmaterial
				* Erstattungskasse für verbrauchsmaterial (dazu später)
				* Matekasse
				* Girokonto mit Rückstellungen (dazu nachher mehr) und Pfandgeld auf Girokonto (letztes konto dieses Jahr neu, damit nicht ~500€ bar im Space rumliegen)
			* viel Vorgenommen, nichts umgesetzt.
			* Viele TOPs für nächsten Schatzmeister
			* Arbeitstreffen wieder auferstanden, sind hilfreich für den vorstand. Prinzipiell auch immer offen für alle anderen Leute!
			* Vierteljährliche Kassenprüfungen sind sehr hilfreich und empfehlenswert für nächsten Vorstand
				* nicht mehr 10 Stunden an einem Stück kurz vor der MV...
			* Sponsoren und Spender: im letzten Jahr keine Firmen :( aber viele Mitgliederspenden für Laseranschaffung
		* Gewinn/Verlust
			* Kapital relativ konstant. Auf der Folie Laserspenden noch nicht vorhanden, weil Statistik im Dezember endet
			* Dip im Oktober: neuer Beamer für 2100€
			* Gesamtkapital: im Moment (Januar 2020) insgesamt über 20.000€ (hier inkl. Laserspenden)
				* Laser liegt bei ~12.5k€ inkl Zubehör
			* monatliche einnahmen: regelmäßige Mitgliedsbeiträge etwas höher, 1642€ pro monat durchschnitt, dafür 138€ spenden durchschnitt pro monat etwas weniger als letzes jahr.
				* kleinere einnahmen bei 3d-druck, freifunk, plotter, stickmaschinenkasse (nichts über ~30€ pro monat)
				* spendendosen sind nicht mehr zweckgebunden, existieren nur noch fürs Tracking!
			* monatliche ausgaben: Miete/nebenkosten sehr konstant bei ~1000€/monat
				* kleinvieh: kontoführungsgebühren ~20€/monat, verbrauchsmaterial schwankt zwischen 0 und ~120€ pro monat
					* kontoführung skaliert mit anzahl buchung auf dem Konto  --> monatliche überweisungen von mitgliedsbeitrag schlucken schon etwa 10 cent pro überweisung
						* viertel/jährliche zahlung spart, auch Aufwand bei der Kassenprüfung
					* verein allgemein: rechnung vom notar / amtsgericht kam erst nach dem Auswertungszeitraum, deswegen hier 0€
			* gegenüberwstellung: mittel 01.2019 bis 01.2020:
				* einnahmen mitgliedsbeiträge und spenden (siehe oben)
				* verpfliuchtungen: 1112 € / monat
					* miete 730
					* strom 285
					* internet 20
					* server 64
					* haftpflichtversicherung 12
					* domain 1€
			* vergleich zum letzten jahr leichter aufwärtstrend bei einnahmen
				* noch ohne die mitgliederbeiträge bei der heutigen MV :)
			* Mitgliederentwicklung:
				* 105 leute am anfang des jahres
				* leicht steigend, heute bei 113 leute
				* einige austritte, aber durch eintritte ausgeglichen
				* vermutung: viele studenten haben inzwischen jobs, anshcluss zur Uni fehlt, also keine neuen mitglieder mehr aus den studis. man könnte mal werbung machen :)
			* matekasse:
				* mitte des jahres irgendwann eine party, deswegen spike nach oben.
				* anmerkung: emantor hat mit finanzamtsleuten gesprochen, die meinen, man darf wirtschaftlichen geschäftsbetrieb nicht mit idellem teil finanzieren, ergo: matekasse darf keinen verlust machen!
				* insgesamt etwa 600€ aus der matekasse rausgenommen, also gesund.
				* trotzdem gibt es leute, die mit ~50€ in der kreide stehen. bezahlt eure matekasseschulden!
		* Bestände
			* rückstellungen:
				* mitsicherheit (sollte schon lange aufgelöst weden) 160.00
				* einnahmeausfall 3000€ (wie sonst bisher auch)
		* Forecast
			* mitgliederbeiträge im aufwind
			* spenden für laser sind eingegangen (auch im mottleren dreistelligen bereich von eizelpersonen)
			* stromverbrauch weiterhin bei ~11.000 kWh pro jahr, aber recht konstant
			* nebenkosten 2018: erstattung von 80€!
			* zweckgebundene posten: aktuell keine. Nein stimmt nicht, es gibt freifunk und coderdojo. aber im moment etwa nur 200€ dafür rumliegen.
				* anmerkung aus dem gespräch mit finanzamtsleuten: wenn spendenbescheinigung ausgestellt wurde, dann kann der verein euch die spende nicht zurückgeben. in diesem fall nur moralische verpflichtung des vereins, das geld zweckmäßig zu verwenden. also nicht so eng sehen wie bisher. beispiel: wenn spende für "beamer" reinkommt, dann darf man das auch für z.b. nähgarn einsetzen, wenn beamer schon durchfinanziert wurde (und der zweck damit weggefallen ist)
			* erstattungskasse geändert: nicht nur verbrauchsmaterial sondern auch beliebige dinge (mit direktem bezug vom space) bis 50€, wenn 4 mitglieder zustimmen bzw bis 30€ wenn 3 mitglieder zustimmen.
				* beispiel: neue leiter für den space, putzmaterial, leuchtmittel, material zum löten...
				* ihr könnt euch auch nur den geldbetrag selbst erstatten, wenn der auch noch in der kasse vorhanden ist!!! (sonst warten, bis schatzmeister neu auffüllt)
				* liste von dingen die erstattet wurden:
					* ~27 euro hardware für die neue trittleiter in der werkstatt
					* 18€ dymo bänder
					* 30€ button-rohlinge
					* 9€ für tonerchips für den drucker)
					* 49€ für granitmessplatte 
					* 15€ lüfter für 3d-drucker
					* 13€ für rollensetzs (zb für plotter)
					* 4.50 müllsäcke
					* 32€ für rollböcke für die werkstatt
		* Fragen:
			* kontogebühren sind ja sehr ärgerlich. kann man da was machen?
				* kontowechsel mehrmals im vorstand diskutiert: gibt nichts, was sinnvoll billiger ist :( auch andere modelle von sparkasse schon durchgerechnet, aber bringt keinen vorteil.
				* mitglieder können weniger buchungen verursachen (zb jährliche überweisung von beitrag)
	* Bericht der Rechnungsprüfer (sonnenschein und shoragan)
		* haben vierteljährlich geprüft, sind gut vorangekommen. auf jeden fall empfehlenswert für nächstes jahr!
		* zwei kleinere todos, aber weniger als im letzten jahr, beide in bearbeitung. einmal 33€ zuviel für nebenkosten überwiesen, das andere ist eine altlast von letztem jahr, fehlender papier-kontoauszug.
		* hinweise:
			* matekasse schulden sollten im einstelligen euro-bereich sein!
			* telefon: anrufe vom space auf handy kosten uns geld. nur so ein hinweis. es  gibt eine spendenkasse im flur :)
				* alleine diesen monat 6€
				* hinweis ist jetzt auch am telefon.


	* Tätigkeitsbericht des Vorstands (chrissi)
		* funktionen im verein:
			* rechnungsprüfer wie oben
			* daniel sturm als vertrauensperson (heute nicht hier, aber hat bericht geschickt)
			* vorstand (siehe wikiseite https://stratum0.org/wiki/Vorstand)
				* davon gemeinsam vertretungsberechtigt: larsan, reneger, emantor (mindestens zwei davon zusammen) (auf der folie steht "zeichnungsberechtigt", das ist nicht der satzungswortlaut)
				* schatzmeister: emantor, schatzmeisterhilfe (laut GO Vorstand): linda
				* verwaltung von schlüsseln: larsan (physisch und SSH-zugang)
				* mitgliederverwaltung: emantor, chrissi^
				* schriftführung: chrissi
		* in zahlen dieses jahr:
			* 5 sitzungen
			* >1250 mails in >420 threads (is mitte dieser woche) auf vorstand@stratum0.org (in etwa im mittel der letzten jahre)
			* 47 umlaufbeschlüsse 8etwas weniger wegen erstattungskassenregelung)
			* 35 seiten  vorstandssitzungsprotokolle (wieder als PDF)
		* steuererklärung
			* für vereine unser größe alle drei jahre notwendig (aufforderung vom finanzamt)
				* finanzamt stelle rückwirkend die gemeinnützigkeit fest
					* doof, wenns schief geht wegen rückerstattung von spenden
			* massig unterlagen: jahresberichte, gewoinn/verlustrechnungen, alle protokolle
				* finanzamt so: sooo viele seiten. sooo viele graphen, und wer ist eigentlich diese herr gottschalk?!
					* G/V-Rechnung und MV-Protokolle hat eigentlich gereicht, Rest brauchen sie nicht unbedingt
					* weniger Papier bitte, "aber die Bäume" (war dieses jahr aber noch auf papier notwendig, demnächst vermutlich als PDF?)
					* "Schreibst du bitte ein How To Steuererklärung?"
			* neuer rekord: 191 seiten dieses jahr (120 MB als PDF) (letztes mal 156 seiten)
			* geht online über Elster
		* öffentliche protokolle
			* interne version wird intern abgelegt, öffentlicher teil nur ohne datenschutzrelevante infos
			* in den letzten jahren eher vorstand als black box
			* dieses jahr die öffentliche version wieder als PDF ins wiki
		* besondere themen:
			* inhaltsversicherung (zusätzlich zu haftpflicht)
				* absicherung von elementarschäden (zb wasserrohrbruch), und im fall von diebstahl
				* nicht ganz trivial, weil viel dinge hier sind, die privatpersonen oder der SMFW UG gehören
				* bisher kein abschluss dazu, trotz viel befassung im vorstand
			* erstattungskasse (siehe oben. sie ist selbstdokumentierend)
			* lasercutter
				* weitergeführt in einer arbeitsgruppe, die anforderungen aufgestellt und lieferanten ausgesucht hat
				* lange sommerpause, danach kontroverse diskussion
				* kosten etwa 11.000€ anschaffung, 1500 € zubehör (abluft, kernbohrung für abluft, einbau in den space: passt nicht durch die tür im frickelraum... tür rausnehmen und neu einsetzen notwendig)
				* stand 12/2019: frei verfügbares kapital > 13.000€ auf konto
				* aktueller vorstandsbeschluss:
					* wenn 3000€ spenden zusammen kommen, werden 12.600€ für anschaffung von lasercutter mit zubehör [aus vereinsmitteln] zur verfügung gestellt
			* keine vorgedruckten papiermitgliedsanträge
				* zusätzliche auffwand (scan, mail)
				* wahrscheinlichkeit, dass es liegen bleibt
				* texte auf formular waren veraltet, keine datenschutzhinweise
			* arbeitstreffen monatlich am 1. dienstag im monat
				* zwischen den vorstandssitzungen, im endeffekt öffentliche dinge, auch gerne TODOs für mitglieder
				* anmerkung dass auch vorstandssitzungen für mitglieder öffentlich sind
			* Eintragung beim OLG Oldenburg in das verzeichnis der gemeinnützigen einrichtungen
				* als möglicher empfänger von geldauflagen bei ermittlungs-, straf- und gnadenverfahren
			* weitere informationen: (folie mit links ins wiki)
				* https://stratum0.org/wiki/Arbeitsgruppe:Gesch%C3%A4ftsf%C3%BChrung#Material_f%C3%BCr_die_Vorstandsarbeit
		* Frage: was sind laufende kosten für lasercutter
			* mittlerer dreistelligen bis vierstelligen betrag pro jahr für filterkarusche, neue laserquelle
			* plan: pro minute nutzung etwa 0.5-1€ als spendenempfehlung
		* was kann man mit lasercutter machen?
			* gravieren, schneiden, zb sperrholz, glas, leder, papier, plastik (falls ungiftig), aber kein metall schneiden weil aufpreis (jedoch gravieren möglich)
			* bearbeitungshöhe bis ~28cm, bettgröße 100cm * 80 cm (ohne seitliche öffnung zum reinschieben wegen laserklasse)
		* laser lieferzeit von etwa 3-4 monaten
		* steht der laser nach einem jahr eher nur noch rum, wenn die euphorie verflogen ist? oder kann man das auch outsourcen?
			* zu 1: hat sich vorstand öfters unterhalten und schätzt rumstehwahrscheinlichkeit sehr gering ein nach vergleich mit anderen hackspaces
			* zu outsourcen: laserbu.de ist zb auch in braunschewig, aber auslastung ist relativ hoch und nicht kurzfristig
				* oder auch protohaus
				* und auch: der space tickt halt so. leute kommen hier spontan her und wir wollen ihnen lösungen bieten.
			* arbeitsgruppe hat benutzerfreundliches modell ausgesucht, keine frickel-selbstbau-lösung, warhscheinlichkeit der nutzung durhc mehrere personen ist damit auch höher.
			* nutzungsverhalten vermutlich wie bei 3d-drucker, zuerst ganz viel und jetzt stetig
		* anmerkung: "wir wollen lasercutter haben" steht seit etwa 7 jahren schon im raum
		* anmerkung: lasercutter sind relativ wertstabil (bis auf die 1500€ für die tür), wird man im notfall auch schnell wieder los
			* arbeitsgruppe hat sich auch nach gebrauchten lasern umgeschaut, daher dieses statement
		* frage: was ist mit space3.0? wird das noch verfolgt? muss man lasercutter dort neu aufbauen?
			* es gab leute, die das verfolgt haben, aber nichts in bezug auf lage / finanzierbarkeit gefunden
				* ~30-40 angebote durchwälzt, 10 davon näher angeschaut
			* wenn wir einen space3.0 haben wollen bei eetwa 200 m², dann ist das etwa 2000-3000€ an Miete. wir sind hier sehr günstig.
			* frage: was war mit dem keller, der im schimmelhof freigeworden ist: (als lagerraum)
				* ist noch auf der todo-liste
			* möglicherweise will prosper-x noch im schimmel-hof expandieren (was heißt das für uns? unklar)
		* frage: wie lange hält so ein laser?
			* laut hersteller: mehrere 100.000 stunden mean time between failure (bis auf verbrauchsmaterial wie laserquelle)
				* alles standardteile, die leicht verfügbar sind



	* Jahresbericht (von larsan)
		* gruppen und regelmäßige veranstaltungen
			* ziemlich voll, wie sonst auch
			* > 30 termine pro monat
			* vorträge, freifunk, coderdojo, digitalcourage/autonymes reisen, coptertreff, malkränzchen, c64 demogruppe, gemeinsame treffen mit ags und akafunk
			* verloren gegangen: home automation: schreibtreff, anime referat, captain's log
			* vorträge:
				* jeden 14. im monat
				* dieses jahr ein termin in allen monaten!
				* 40 vorträge insgesamt (davon 9 von drahflow, je 4 oder 5 von emantor und rohieb)
				* recordings: ~30 (die letzten sind noch in der queue)
				* streamingmöclihkit weiterhin angedacht
				* seit anbgeinn der zeit 286 vorträge
			* freifunk immer noch jeden mittwoch
				* >400 router online (trend nach oben)
				* eventwlan läuft auf mehreren veranstaltungen
					* gerne mehr. meldet euch bei uns! ist relativ einfach aufzubauen
				* in zukunft auch in jugendzentren und feuerwehren
				* project parker weiterhin im aufbau (restrukturierung des netzes mit neueren technologien), jetzt in beta-betrieb, vorträge auf mehreren konferenzen von sho, kasa, chrissi
			* coderdojo: 11 termine, davon 3 extern (msg david, triology, cloudogu, prosper-x (kurzfristig im schimmelhof eingemietet)
				* weiterhin mentoren gesucht! -> https://lists.stratum0.org/mailman/listinfo/dojoorga
		* foodstuffs: workshops zu bier brauen, wurst
			* vegan workbench und sushiworkshops teilweise eingeschlafen?
			* wurstfleischmischer hat einen motor bekommen
			* stratum {Donut}: donutmaschine privat gecrowdfundet
		* workshops:
			* pflanzenmilch blindverkostung (etwa 40 sorten)
			* dsgvo-nachmittag
			* Virtual-Reality-wochenende
		* infrastruktur
			* kaum was neues
			* infrastructure review: wer betreut eigentlich welche server und was braucht man noch?
			* NAS hat ein neues gehäuse
			* matekasse-system im rewrite, testbetrieb auf http://matekasse-test.s0, kontakt: chaosrambo
		* 8 jahre stratum 0
			* full space: 96 tickets wurden geklickt
			* bar und essen 
			* zwei sorten selbstgemachtes bier
		* events auswärts: easterhegg, gpn19, cccamp, 36c3, mit jeweils einigen leuten von uns
			* [insert congressbilder here]
			* camp: https://singularitycity.de/ zusammen mit anderen spaces zusammen, 12-14 gruppen, > 160 entitäten
			* [insert camp-bilder here]
		* misc
			* 1 ausgabe stratumnews!
			* gerne auch mehr blogposts (nicht nur von chrissi)
			* 1 spacebau-abend
				* kühlschrank hat jetzt WLAN
		* anträge
			* mitgliedantrag auf sanifair-voucher
		* ausblick 2020
			* LASER
			* space 2 trägt hoffentlich mehr früchte
			* HOA 2020 https://hackenopenair.de/
			* neue Shirts wurden in die wege geleitet, es gibt testshirts zum anprobieren
	* Bericht der Vertrauensentität
		* via reneger, weil dstulle nicht anwesend
		* es gibt nichts zu berichten, es gab keine anfragen und keine anlässe, tätig zu werden
		* frage: was ist mit dem konflikt vom letzten jahr?
			* vorstand sieht keinen handlungsbedarf, weil alles gegen privatpersonen geht.
				* ein schlüssel für fahrradkeller wurde nicht zurückzugeben, vorstand hat mehrmals nachgefragt, aber inzwischen abgeschrieben
			* es gab mehrere gespräche dritter personen dazu im chat, bei denen daten von mitgliedern geleakt wurden
				* vorstand sieht da keine möglichkeit, etwas zu tun, bans sind leicht auszuhebeln, und rechtlich nicht erreichbar weil außerhalb EU
				* es gab offenbar ein google doc im chat? vorstand weiß nichts davon?
			* frage: wurde vorstand aktiv dazu angesprochen mit aufforderung was zu tun?
				* war nicht der fall.
			* vorstand hat teilweise mehrere stunden pro monat auf diesen fall verwendet, "da kann man unendlich viel zeit reintun und das ergebnis ist null."

*TOP 2: Entlastung des Vorstands
	* kassenprüfer empfehlen entlastung.
	* jemand stellt den antrag, den vorstand gesamt für die letzte vorstandsperiode zu entlasten
		* 31 stimmberechtigte anwesend, vorstand stimmt nicht mit ab
		* entlastung ansonsten einstimmig angenommen

*TOP 3: Wahlen
16:05
	* Wahlleitung: Vorschlag: Emantor. keine gegenstimmen
	* Übergabe an die wahlleitung
	* wahlleiter fragt nach weiteren kandidaten
		* sollen wir rechnungsprüfer neu wählen?
			* rechnungsprüfer wollen weitermachen, und niemand will sie neu wählen, werden also nicht neu gewählt.
kandidatenliste wird geschlossen
wahlleitung erklärt den wahlmodus laut mv-geschäftsordnung
kandidaten stellen sich kurz vor
	wahlhelfer: drc, shim, emil, der bart, claudia, nora, mjh und ulif

ergebnis wird um 17:23 verkündet
Kandidierende Entitäten:
	* Vorsitzende Entität („Vorstandsvorsitzender“)
		* larsan: 30/35 stimmen, nimmt die wahl an
	* Stellv. vorsitzende Entität („stellv. Vorsitzender“)
		* chrissi zieht kandidatur als stellv. vorsitzende  zurück
		* feli ist gewählt mit 26 stimmen, nimmt wahl an
	* Geld verwaltende Entität („Schatzmeister“)
		* helena: nimmt wahl an, mit 30 stimmen gewählt
	* Beisitzende Entitäten („Beisitzer“)
		* larsan ist schon gewählt (31 stimmen)
		* feli (30 stimmen) ist schon gewählt
		* chrissi: 29 stimmen, nimmt wahl an
		* linda: 29 stimmen, nimmt wahl an
		* rohieb (27 stimmen), nimmt wahl an
	* Der Geld verwaltenden Entität auf die Finger schauende Entitäten („Rechnungsprüfer“, falls Neuwahl gewünscht)
		* wurden nicht gewählt

Rückgabe an die versammlungsleitung.

*TOP 4: Sonstiges
	* Benennung der Vertrauensentität (falls neue Benennnug gewünscht)
		* ist nicht anwesend, aber würde das amt weiter machen
		* niemand möchte die vertrauensentität neu wählen
	* Terminfindung nächste MV
		* sonntag 17. januar 2021
		* vorschlag für ankündigungstermin: 1. januar 2021
	* dank an alle vorstandsmitglieder, die sich nicht im neuen vorstand wiederfinden
	* Anerkennung des RaumZeitLabors als Außenstelle des Stratum 0
		* vertagt
	* Weltherrschaft
		* vertagt
	* Keysigning-Party
		* bei interesse im anschluss an diese versammlung

gibt es weitere themen?
	* neuer und alter vorstand möge nach dieser MV für einen termin für eine übergabesitzung zusammenfinden

versammlung geschlossen um 17:30
































