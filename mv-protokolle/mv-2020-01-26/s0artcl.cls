%%
%% This is file `s0artcl.cls',
%% generated with the docstrip utility.
%%
%% The original source files were:
%%
%% s0artcl.dtx  (with options: `class')
%% 
%% Copyright (C) 2017 Roland Hieber <rohieb+latex@rohieb.name>
%% 
%% This file may be distributed and/or modified under the
%% conditions of the LaTeX Project Public License, either
%% version 1.3 of this license or (at your option) any later
%% version. The latest version of this license is in:
%% 
%%   http://www.latex-project.org/lppl.txt
%% 
%% and version 1.3 or later is part of all distributions of
%% LaTeX version 2005/12/01 or later.
%% 
\def\thisclass{s0artcl}
\def\thisfile{\thisclass.dtx}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{\thisclass}
[2018/07/17 v1.1 Stratum 0 Corporate Identity -- Article]
\PassOptionsToClass{a4paper}{scrartcl}
\PassOptionsToClass{11pt}{scrartcl}
\PassOptionsToClass{parskip=half-}{scrartcl}
\DeclareOption*{\InputIfFileExists{\CurrentOption.s0c}{}{%
\PassOptionsToClass{\CurrentOption}{scrartcl}}}
\ProcessOptions \relax
\LoadClass{scrartcl}
\RequirePackage{xpatch}
\RequirePackage{hyperref}
\hypersetup{unicode=true,colorlinks=false,pdfborder=0 0 0}
\urlstyle{sf}
\RequirePackage[left=2.5cm,right=2.5cm,top=2cm,bottom=2.5cm]{geometry}
\RequirePackage{lastpage}
\def\sn@pagenameof@english#1{Page #1 of \pageref{LastPage}}
\let\sn@pagenameof@UKenglish\sn@pagenameof@english
\let\sn@pagenameof@american\sn@pagenameof@english
\let\sn@pagenameof@USenglish\sn@pagenameof@english
\let\sn@pagenameof@british\sn@pagenameof@english
\let\sn@pagenameof@canadian\sn@pagenameof@english
\let\sn@pagenameof@australian\sn@pagenameof@english
\let\sn@pagenameof@newzealand\sn@pagenameof@english
\def\sn@pagenameof@german#1{Seite #1 von \pageref{LastPage}}
\let\sn@pagenameof@ngerman\sn@pagenameof@german
\let\sn@pagenameof@austrian\sn@pagenameof@german
\let\sn@pagenameof@germanb\sn@pagenameof@german
\let\sn@pagenameof@naustrian\sn@pagenameof@german
\ifx\languagename\undefined\def\languagename{english}\fi
\RequirePackage[headsepline]{scrlayer-scrpage}[2013/12/27]
\ihead{\textsc{\@title}}
\chead{}
\ohead{\textsc{\@date}}
\ifoot{}
\cfoot*{\csname sn@pagenameof@\languagename\endcsname\thepage}
\ofoot{}
\pagestyle{scrheadings}
\RedeclareSectionCommand[afterskip=0.5em]{section}
\RedeclareSectionCommand[afterskip=0.1em]{subsubsection}
\RedeclareSectionCommand[beforeskip=.5\parskip, afterskip=-1em]{paragraph}
\setlength{\parindent}{0em}
\RequirePackage{enumitem}
\setlist{nosep}
\RequirePackage{tocloft}
\setlength{\cftbeforesecskip}{0.3\baselineskip}
\renewcommand{\cfttoctitlefont}{\usekomafont{section}}
\RequirePackage{yanonekaffeesatzzerohack}
\setkomafont{title}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{b}{n}}
\setkomafont{subtitle}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{n}{n}\Large}
\xpatchcmd{\maketitle}{\huge}{\Huge}{}{}
\xpatchcmd{\@maketitle}{\huge}{\Huge}{}{}
\setkomafont{section}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{b}{n}\huge}
\setkomafont{subsection}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{l}{n}\LARGE}
\setkomafont{subsubsection}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{l}{n}\Large}
\setkomafont{paragraph}{\usefont{LY1}{YanoneKaffeesatzZeroHack}{l}{n}\normalsize}
\renewcommand*{\encodingdefault}{T1}
\RequirePackage[default]{droidsans}
\@ifpackagelater{droidsans}{2019/06/20}{
\RequirePackage[defaultmono]{droidsansmono}
}{
\RequirePackage[defaultmono]{droidmono}
}
\setkomafont{subject}{\normalfont\large}
\setkomafont{author}{\normalfont\large}
\setkomafont{date}{\normalfont\large}
\setkomafont{paragraph}{\normalfont\bfseries}
\setkomafont{caption}{\normalfont\itshape}
\setkomafont{captionlabel}{\normalfont\itshape}
\endinput
%%
%% End of file `s0artcl.cls'.
